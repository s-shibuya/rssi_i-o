package com.ssdev109.irtestapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.AdvertisingSetParameters;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.ConsumerIrManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity{
    ConsumerIrManager ir;
    TextView btTextView;
    TextView bleConnectTextView;
    TextView btLeTextView;
    TextView advertisingTextView;
    Button btButton;
    Button btLeButton;
    Button btLeConnectButton;
    Button advertisingButton;
    Button csvOutputButton;
    Button bleMapClearButton;
    BluetoothAdapter bluetoothAdapter;
    BluetoothAdapter leBluetoothAdapter;
    BluetoothAdapter bleAdapter;
    BluetoothLeScanner bluetoothLeScanner;
    BluetoothLeAdvertiser advertiser;
    ScanCallback scanCallback;
    Switch btSwitch;
    Switch btLeSwitch;
    EditText distanceEditText;
    final Handler handler = new Handler();
    TextView detectedDeviceTextView;
    boolean isLeScan=false;
    Runnable runnable;
    Runnable leRunnable;
    Runnable advRunnable;
    BluetoothManager bleManager;
    Map<String,Map<String,Map<Long,String>>> deviceMap=new LinkedHashMap();
    Map<String,Map<Long,String>> distanceMap=new LinkedHashMap();
    Map<Long,String> timeToRssiMap=new LinkedHashMap();
    boolean advSending=false;
    long scanStartTime;
    File path;
    File file;

    public enum RequestCode{
        bluetoothReq(1);

        private int id;
        RequestCode(int _id){
            this.id=_id;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //赤外線
        ir = (ConsumerIrManager)getSystemService(CONSUMER_IR_SERVICE);
        bleManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        ConsumerIrManager.CarrierFrequencyRange[] ranges=ir.getCarrierFrequencies();
        for(int i=0;i<ranges.length;i++){
            Log.d("ranges","max:"+ranges[i].getMaxFrequency()+",min:"+ranges[i].getMinFrequency());
        }
        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        //btSwitch=findViewById(R.id.btSwitch);
        detectedDeviceTextView=findViewById(R.id.detectedDevice);
        //Bluetooth
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        btTextView=findViewById(R.id.btTxt);
        btButton=findViewById(R.id.btButton);
        btLeButton=findViewById(R.id.btLeButton);
        btLeTextView=findViewById(R.id.btLeTxt);
        advertisingButton=findViewById(R.id.advertisingButton);
        advertisingTextView=findViewById(R.id.advertisingText);
        csvOutputButton=findViewById(R.id.csvOutput);
        distanceEditText=findViewById(R.id.distanceEditText);
        bleMapClearButton=findViewById(R.id.bleMapClear);
       // btLeSwitch=findViewById(R.id.btLeSwitch);
      //  bleConnectTextView=findViewById(R.id.btLeConnectTxt);
      //  btLeConnectButton=findViewById(R.id.btLeConnectButton);
        if (bluetoothAdapter == null) {
            btTextView.setText("this Device is not support Bluetooth");
            btLeTextView.setText("this Device is not support Bluetooth");
            btButton.setEnabled(false);
            btLeButton.setEnabled(false);
        }
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE ) && isCapableToAdvertise(leBluetoothAdapter))
        {
            btLeTextView.setText("this Device is not support BLE");
            advertisingTextView.setText("this Device is not support BLE");
            advertisingButton.setEnabled(false);
            btLeButton.setEnabled(false);
        }
        IntentFilter btFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, btFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    public void irEmit(View view){
        int on_offArray[]={3350,1700, 400,400, 450,1200, 450,400, 450,1250, 400,400, 400,1250, 450,400, 400,1250, 400,450, 400,1250, 400,400, 450,1250, 400,1250, 400,400, 450,1250, 400,400, 450,1250, 400,1250, 400,1300, 400,1200, 450,400, 400,400, 450,400, 450,1250, 400,400, 400,1300, 400,400, 400,400, 450,1250, 400,400, 450,400, 400,400, 450,1250, 400,400, 450,1250, 400,400, 450,1250, 400,400, 450,1250, 400,400, 400,400, 450,1250, 400,400, 450,400, 400,1300, 400,400, 400,400, 450,1250, 400};
        if (ir.hasIrEmitter()) {
            ir.transmit(38000, on_offArray);
        } else {
            Log.d("from irEmit","this device is not support ir");
        }
    }

    public void bluetoothScan(View view){
        long SCAN_PERIOD = 5000;
        isLeScan=false;
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, RequestCode.bluetoothReq.id);
        }else{
            if(bluetoothAdapter.isDiscovering()){
                //前回のhandlerのリセット
                if(runnable!=null){
                    handler.removeCallbacksAndMessages(runnable);
                }
                bluetoothAdapter.cancelDiscovery();
                btButton.setText("SEARCH");
            }else{
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                runnable=new Runnable() {
                    @Override
                    public void run() {
                        // タイムアウト
                        //ボタンが押されるたびに作るbool
                        //ボタンのsearchの文字をcancelに変更、ラベルを~(searching)に変更
                        if (bluetoothAdapter.isDiscovering()) {
                            bluetoothAdapter.cancelDiscovery();
                            btButton.setText("SEARCH");
                        }
                    }
                };
                handler.postDelayed(runnable, SCAN_PERIOD);
                bluetoothAdapter.startDiscovery();
                btButton.setText("CANCEL");
            }
           // bluetoothConnect();
        }
    }

    public void bluetoothLEScan(View view){
        String deviceDitance=distanceEditText.getText().toString();
        if(Build.VERSION.SDK_INT>20){
            newLEScan(deviceDitance);
        }else{
            oldLEScan(deviceDitance);
        }
    }

    public void newLEScan(final String deviceDistance){
        long SCAN_PERIOD = 20000;
        isLeScan=true;
        if(!bluetoothAdapter.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, RequestCode.bluetoothReq.id);
        }else{
            if(leRunnable!=null){
                //前回のhandlerのリセット
                handler.removeCallbacksAndMessages(leRunnable);
                bluetoothLeScanner.stopScan(scanCallback);
                btLeButton.setText("SEARCH");
                leRunnable=null;
                if(distanceMap.size()>0){
                    csvOutputButton.setEnabled(true);
                }
            }else{
                scanStartTime=System.currentTimeMillis();
                leBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                leRunnable=new Runnable() {
                    @Override
                    public void run() {
                        // タイムアウト
                        bluetoothLeScanner.stopScan(scanCallback);
                        btLeButton.setText("SEARCH");
                        leRunnable=null;
                        String resultInfo="";
                        for(Map.Entry<String, Map<String,Map<Long,String>>> deviceEntry : deviceMap.entrySet()){
                            resultInfo+="\n"+deviceEntry.getKey()+"\n";
                            for(Map.Entry<String,Map<Long,String>> distanceEntry : deviceEntry.getValue().entrySet()){
                                resultInfo+=distanceEntry.getKey()+"cm\n";
                                for(Map.Entry<Long,String> timeEntry : distanceEntry.getValue().entrySet()){
                                    resultInfo+=timeEntry.getKey().toString()+"ms:"+timeEntry.getValue()+"dB\n";
                                }
                            }
                        }
                        if(deviceMap.size()>0){
                            csvOutputButton.setEnabled(true);
                            bleMapClearButton.setEnabled(true);
                        }
                        detectedDeviceTextView.setText(resultInfo);
                    }
                };
                bluetoothLeScanner = leBluetoothAdapter.getBluetoothLeScanner();
                scanCallback = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        super.onScanResult(callbackType, result);
                        Log.d("onScan",Integer.toString(result.getRssi()));
                        BluetoothDevice bluetoothDevice = result.getDevice();
                        //deviceName取得
                        String deviceName=bluetoothDevice.getName();
                        if(deviceName==null){
                            deviceName=bluetoothDevice.getAddress();
                        }
                        distanceMap=deviceMap.get(deviceName);
                        if(distanceMap==null){
                            //time,rssのMap
                            timeToRssiMap =new LinkedHashMap<>();
                            timeToRssiMap.put((System.currentTimeMillis()-scanStartTime),Integer.toString(result.getRssi()));
                            distanceMap=new LinkedHashMap<>();
                            distanceMap.put(deviceDistance,timeToRssiMap);
                        }else{
                            timeToRssiMap=distanceMap.get(deviceDistance);
                            if(timeToRssiMap==null){
                                timeToRssiMap =new LinkedHashMap<>();
                            }
                            timeToRssiMap.put((System.currentTimeMillis()-scanStartTime),Integer.toString(result.getRssi()));
                            distanceMap.put(deviceDistance,timeToRssiMap);
                        }
                        deviceMap.put(deviceName,distanceMap);
                    }
                };
                ScanSettings.Builder scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
                handler.postDelayed(leRunnable, SCAN_PERIOD);
                ScanFilter scanFilter = new ScanFilter.Builder().setServiceUuid(new ParcelUuid(UUID.fromString("65432461-1EFE-4ADB-BC7E-9F7F8E27FDC1"))).build();
                ArrayList scanFilterList = new ArrayList();
                scanFilterList.add(scanFilter);
                bluetoothLeScanner.startScan(scanFilterList,scanSettings.build(), scanCallback);
                btLeButton.setText("CANCEL");
            }
        }
    }

    public void oldLEScan(final String deviceDistance){
        long SCAN_PERIOD = 5000;
        isLeScan=true;
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, RequestCode.bluetoothReq.id);
        }else{
            if(leRunnable!=null){
                //前回のhandlerのリセット
                handler.removeCallbacksAndMessages(leRunnable);
                leBluetoothAdapter.stopLeScan(new BluetoothAdapter.LeScanCallback() {
                    @Override
                    public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {}
                });
                btLeButton.setText("SEARCH");
                leRunnable=null;
            }else{
                timeToRssiMap =new LinkedHashMap<>();
                scanStartTime=System.currentTimeMillis();
                leBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                //final BluetoothLeScanner bluetoothLeScanner = leBluetoothAdapter.getBluetoothLeScanner();
                leRunnable=new Runnable() {
                    @Override
                    public void run() {
                        // タイムアウト
                        leBluetoothAdapter.stopLeScan(new BluetoothAdapter.LeScanCallback() {
                            @Override
                            public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {}
                        });
                        btLeButton.setText("SEARCH");
                        leRunnable=null;
                        String resultInfo="";
                        for(Map.Entry<String, Map<String,Map<Long,String>>> deviceEntry : deviceMap.entrySet()){
                            resultInfo+="\n"+deviceEntry.getKey()+"\n";
                            for(Map.Entry<String,Map<Long,String>> distanceEntry : deviceEntry.getValue().entrySet()){
                                resultInfo+=distanceEntry.getKey()+"cm\n";
                                for(Map.Entry<Long,String> timeEntry : distanceEntry.getValue().entrySet()){
                                    resultInfo+=timeEntry.getKey().toString()+"ms:"+timeEntry.getValue()+"dB\n";
                                }
                            }
                        }
                        if(deviceMap.size()>0){
                            csvOutputButton.setEnabled(true);
                            bleMapClearButton.setEnabled(true);
                        }
                        detectedDeviceTextView.setText(resultInfo);
                    }
                };
                handler.postDelayed(leRunnable, SCAN_PERIOD);
                bluetoothAdapter.startLeScan(new BluetoothAdapter.LeScanCallback() {
                    @Override
                    public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {
                        //time,rssのMap
                        timeToRssiMap.put((System.currentTimeMillis()-scanStartTime),Integer.toString(rssi));
                        //distance,timeToRssiMapのMap(distanceMap)を更新
                        distanceMap.put(deviceDistance,timeToRssiMap);
                        //deviceName,distanceMapのMap(deviceMap)を更新
                        if(bluetoothDevice.getName()!=null){
                            deviceMap.put(bluetoothDevice.getName(),distanceMap);
                        }else{
                            deviceMap.put(bluetoothDevice.getAddress(),distanceMap);
                        }
                    }
                });
                btLeButton.setText("CANCEL");
            }
            // bluetoothConnect();
        }
    }

        public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public String generateCsv(){
        String csvString="";
        for(Map.Entry<String, Map<String,Map<Long,String>>> deviceEntry : deviceMap.entrySet()){
            csvString+="device,"+deviceEntry.getKey()+"\n";
            for(Map.Entry<String,Map<Long,String>> distanceEntry : deviceEntry.getValue().entrySet()){
                csvString+="distance(cm),"+distanceEntry.getKey()+"\n";
                csvString+="time(ms),";
                for (Long time : distanceEntry.getValue().keySet()) {
                    csvString+=time+",";
                }
                csvString+="\nRSSI(dB),";
                for (String rssi : distanceEntry.getValue().values()) {
                    csvString+=rssi+",";
                }
                csvString+="\n";
            }
            csvString+="\n";
        }
        return csvString;
    }

    public void manageCsv(View view){
        switch(view.getId()){
            case R.id.csvOutput:{
                //パーミッションの確認
                if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    //許可が出ていた場合
                    if(path==null){
                        path = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                        file = new File(path, "ble.csv");
                    }
                    try{
                        Log.d("csv",Boolean.toString(isExternalStorageWritable()));
                        FileOutputStream fileOutputStream = openFileOutput( "ble.csv", MODE_PRIVATE );
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
                        BufferedWriter bw = new BufferedWriter(outputStreamWriter);
                        bw.write(generateCsv());
                        bw.flush();
                        Toast toast = Toast.makeText(this, "保存完了", Toast.LENGTH_SHORT);
                        toast.show();
                    } catch (Exception e) {
                        Log.d("csv",e.toString());
                    }
                }else{
                    //出ていなかった場合
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 10);
                }
                break;}
            case R.id.bleMapClear:{
                deviceMap=new LinkedHashMap<>();
                distanceMap=new LinkedHashMap<>();
                timeToRssiMap=new LinkedHashMap<>();
                csvOutputButton.setEnabled(false);
                bleMapClearButton.setEnabled(false);
                break;}
        }
    }

    public boolean isCapableToAdvertise(BluetoothAdapter bAdapter){
        return Build.VERSION.SDK_INT > 20 &&
                null != bAdapter.getBluetoothLeAdvertiser() &&
                bAdapter.isMultipleAdvertisementSupported() &&
                bAdapter.isOffloadedFilteringSupported() &&
                bAdapter.isOffloadedScanBatchingSupported();
    }

    public void startAdvertising(View view){

        if(advRunnable!=null){
            handler.removeCallbacksAndMessages(advRunnable);
            advertiser.stopAdvertising(new AdvertiseCallback() {});
            advertisingButton.setText("START");
            advRunnable=null;
        }else{
            BluetoothAdapter adapter = bleManager.getAdapter();
            advertiser = adapter.getBluetoothLeAdvertiser();
            advRunnable=new Runnable() {
                @Override
                public void run() {
                    // タイムアウト
                    advertiser.stopAdvertising(new AdvertiseCallback() {});
                    advertisingButton.setText("START");
                    advRunnable=null;
                }
            };
            // 設定
            AdvertiseSettings.Builder settingBuilder = new AdvertiseSettings.Builder();
            settingBuilder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER);
            settingBuilder.setConnectable(true);
            settingBuilder.setTimeout(0);
            settingBuilder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW);
            AdvertiseSettings settings = settingBuilder.build();

            // アドバタイジングデータ
            AdvertiseData.Builder dataBuilder = new AdvertiseData.Builder();
            dataBuilder.addServiceUuid(new ParcelUuid(UUID.fromString("65432461-1EFE-4ADB-BC7E-9F7F8E27FDC1")));
            AdvertiseData advertiseData = dataBuilder.build();

            //レスポンスデータ
            AdvertiseData.Builder respBuilder = new AdvertiseData.Builder();
            respBuilder.setIncludeDeviceName(true);
            AdvertiseData respData=respBuilder.build();

            //アドバタイズを開始
            handler.postDelayed(advRunnable, 60000);
            advertiser.startAdvertising(settings, advertiseData, respData, new AdvertiseCallback() {
                @Override
                public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                    super.onStartSuccess(settingsInEffect);
                }

                @Override
                public void onStartFailure(int errorCode) {
                    super.onStartFailure(errorCode);
                }
            });
            advertisingButton.setText("CANCEL");
        }
    }

    public void bluetoothConnect(){
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                Log.d("from bluetoothConnect",deviceName);
                if(btSwitch.isChecked()){
                    //new AcceptThread(bluetoothAdapter,handler).start();
                }else{
                    //new ConnectThread(device,bluetoothAdapter,handler).start();
                }
            }
        }else{
            Log.d("from bluetoothConnect",Boolean.toString(bluetoothAdapter.startDiscovery()));
//            Intent discoverableIntent =
//                    new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//            //discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
//            startActivity(discoverableIntent);
        }
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("from BroadCastReceiver",intent.toString());
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                if(deviceName!=null){
                    String currentText=detectedDeviceTextView.getText().toString();
                    currentText+="\n"+deviceName+":RSSI"+rssi;
                    detectedDeviceTextView.setText(currentText);
                    Log.d("from BroadCastReceiver",deviceName+":RSSI"+rssi);
                }
            }
        }
    };

    public void turnBLESwitch(View view){
        if(btLeSwitch.isChecked()){
            //server
            bleConnectTextView.setText("Bluetooth LE Server Peripheral");
        }else{
            //client
            bleConnectTextView.setText("Bluetooth LE Client Central");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 10:{
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    manageCsv(csvOutputButton); }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case 1:{
                if(resultCode==RESULT_OK){
                    if(isLeScan){
                        bluetoothLEScan(btButton);
                        isLeScan=false;
                    }else{
                        bluetoothScan(btButton);
                    }
                    //bluetoothConnect();
                }else{
                    return;
                }
                break;
            }
        }
    }
}

