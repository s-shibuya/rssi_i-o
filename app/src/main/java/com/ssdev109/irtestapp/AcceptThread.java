package com.ssdev109.irtestapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;


public class AcceptThread extends Thread {
    private final BluetoothServerSocket mmServerSocket;
    private final BluetoothAdapter bluetoothAdapter;
    public final Handler handler;

    public AcceptThread(BluetoothAdapter _bluetoothAdapter, Handler _handler) {
        // Use a temporary object that is later assigned to mmServerSocket
        // because mmServerSocket is final.
        handler=_handler;
        bluetoothAdapter=_bluetoothAdapter;
        BluetoothServerSocket tmp = null;
        try {
            // MY_UUID is the app's UUID string, also used by the client code.
            tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord("irtestapp", UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
        } catch (IOException e) {
            Log.e("AcceptThread", "Socket's listen() method failed", e);
        }
        mmServerSocket = tmp;
    }

    public void run() {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned.
        while (true) {
            try {
                socket = mmServerSocket.accept();
            } catch (IOException e) {
                Log.e("AcceptThread", "Socket's accept() method failed", e);
                break;
            }

            if (socket != null) {
                // A connection was accepted. Perform work associated with
                // the connection in a separate thread.
                MyBluetoothService myBluetoothService=new MyBluetoothService(handler);
                myBluetoothService.new ConnectedThread(socket,true).start();
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    Log.d("serverSocket.close:",e.toString());
                }
                break;
            }
        }
    }

    // Closes the connect socket and causes the thread to finish.
    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) {
            Log.e("AcceptThread", "Could not close the connect socket", e);
        }
    }
}